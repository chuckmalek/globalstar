
#
#  GlobalStar LTE and WiFi vs. WiFi automated interference testing Intelligence with *.csv output.
# by Charles J Malek, P.E. Property of Roberson & Associates. October 2017
# ver 1.0a on 26 October 2017. added iPerf exception handling and breaks.
# ver 1.0b 26 October. jammer range and RSSI missing terms.
# ver 1.0c 27 October adjusted AP attenuation list
# ver 1.0c 30 Oct 2017 stabilization waiting time for first lowest level jammer.
# ver 2.0 11 Dec 2017.  John's timing sleeps. Kbps to Mbps iPerf conversion.
#
import sys,csv,datetime, string, os, telnetlib
from ctypes import * 
import scipy
import numpy as np
import subprocess as SP
import time

def get_iPerf_TCP():
    cmdTCP = ['iperf3.exe', '-c', '192.168.1.7', '-O 3', '-t 30']  # TCP/IP is the default
    try:
        output1=SP.check_output(cmdTCP)
    except FileNotFoundError as e:
        print('Error opening %s, must be reachable from path env. variable ' % cmd[0],e)
    except:
        ("iPerf TCP fails.\n")
        return(0)
    outputstrings=output1.decode('utf-8').split('\n')
#   for ss in outputstrings:
#      print(ss)
#    print (outputstrings[16],"\n\n")  
    BW = float(outputstrings[38].split()[6])
	if (BW > 65):
		BW = BW * 0.001				# Kbps to Mbps conversion
    print("TCP_BW = ", BW, " Mbps")
    return(BW)
 
def get_iPerf_UDP():
    cmdUDP = ['iperf3.exe', '-c', '192.168.1.7', '-u', '-b 54M', '-t 20' ] # UDP at 54 Mbits/sec offered load
    try:
        output1=SP.check_output(cmdUDP)
    except FileNotFoundError as e:
        print('Error opening %s, must be reachable from path env. variable ' % cmd[0],e)
    except:
        print("iPerf for UDP returns 100% PER.\n")
        udpPER = 100
        udpBW = 0
        return(udpPER,udpBW)
    outputstrings=output1.decode('utf-8').split('\n')
#    for ss in outputstrings:
#        print(ss)
    udpPER = float(outputstrings[25].split()[11].replace("%)", "").replace("(", ""))
    udpBW = float(outputstrings[25].split()[6])
    print("udpPER =", udpPER, "%", " UDP BW =", udpBW, "Mbps \n")
    return(udpPER,udpBW)      

def setJammerGenerator(freq, power):
# sets power and freq on the SMBV-100A vector signal jammer generator
    host = "192.168.1.5"
    port = "5025"
    telnet = telnetlib.Telnet(host,port)
#    print(telnet)
    print ("configuring generator power to", power, "dBm and freq to",freq, "GHz ")
    telnet.write(b"source1:pow" + b" " + power.encode('ascii') + b"\r\n")
    telnet.write( b"source1:freq" + b" " + freq.encode('ascii') + b"GHz" + b"\r\n")
    telnet.close()  
    
def reset_jammerGenerator():
    host = "192.168.1.5"
    port = "5025"
    telnet = telnetlib.Telnet(host,port)
    print(telnet)
    print ("Resetting the SMBV-100A")
    telnet.write(b"*RST\n")   # resets the SMBV-100A vector signal jammer generator
    telnet.write(b"MMEM:LOAD:STAT 4,'/var/user/globalstarWIFI_CH6' \n") #pre-load memory locations from pathnames
    telnet.write(b"MMEM:LOAD:STAT 5, '/var/user/Unicod/TDD_DL_80PCT' \n")
    telnet.write(b"MMEM:LOAD:STAT 6, '/var/user/Unicod/TDD_UL_40PCT' \n")
    telnet.close()
    
def setString_jammerGenerator(anyString):
    # resets the SMBV-100A vector signal jammer generator
    host = "192.168.1.5"
    port = "5025"
    telnet = telnetlib.Telnet(host,port)
    print(telnet)
    print ("sending string", anyString, " to SMBV-100A.\n")
    telnet.write(anyString.encode('ascii') + b"\r\n")
    telnet.close()
 
def Vaunix_init(attenuation):
# The LDA-602 has a 60 dB range,uses 0.5 dB steps,has an approx 7.5 dB insertion Loss at 2.4 GHz.               
    vnx = cdll.VNX_atten64
    vnx.fnLDA_SetTestMode(0)        # turn off the DLL’s simulation test mode
    DeviceIDArray = c_int * 64      # the array just has to be big enough for the number of devices you expect – 20 is fine, 64 is probably more than you need
    Devices = DeviceIDArray()
    vnx.fnLDA_GetNumDevices()      # tell the DLL to go out and find the hardware, it returns the number of devices found
    # at this point you can use the handles stored in the array to get information about each connected device
    vnx.fnLDA_GetDevInfo(Devices)    # get a list of active device handles from the DLL
    vnx.fnLDA_GetSerialNumber(Devices[0])   # get the serial number of the first connected device
    # for each device you want to use, init the device, or devices. My example assumes two devices
    vnx.fnLDA_InitDevice(Devices[0])    #initialize attenuation
    vnx.fnLDA_SetAttenuation(Devices[0], attenuation * 4)  # the DLL uses an integer value with units of .5 db, so the value is multiplied by 4
    # the python ctypes library does the conversion from python data types to integers
    print ("configuring Vaunix attenuator to ", attenuation, "dB ")
    vnx.fnLDA_CloseDevice(Devices[0])
    
def main():
# Greetings. The testing intelligence asks for human measurement input.
    print("\n\n")
    print("We welcome you to the GlobalStar terrestrial Testing Intelligence! \n")
    print("Please give the model of AP. \n\n")
    AP = input()
    print("Specify the AP to client distance in meters:")
    d_APtoClient = float(input())
    print("Specify the jammer to client distance in meters:")
    d_jammerToClient = float(input())
    print("Specify the AP to client insertionLoss, path Loss, plus antenna Gains:")
    pathLossAP2client = float(input())
    print("Specify the jammer to client insertion Loss, path Loss, plus antenna Gains:")
    pathLossJammer2client = float(input())
    print("Specify AP channel 1 power output in dBm:")
    APpower1 = float(input())
    print("Specify AP channel 6 power output in dBm:")
    APpower6 = float(input())    
    print("Specify AP channel 11 power output in dBm:")
    APpower11 = float(input())
    print("Specify the RSSI error in dBm:")
    RSSIerror = float(input())
    
    print(sys.version) 
    dt= datetime.datetime.now()
    print(dt)
    
    outputFile = open('globalStarDay0.csv','w', newline='')
    outputWriter = csv.writer(outputFile)
    outputWriter.writerow(['GlobalStar', '2.4 GHz', 'Residential Test']) 
    outputWriter.writerow(['stardate = ', dt.year,dt.month,dt.day,dt.hour,dt.minute])
    outputWriter.writerow([" "])
    outputWriter.writerow([' AP: ',AP, 'distance AP to client: ', d_APtoClient,'distance jammer to client:', d_jammerToClient])
    outputWriter.writerow(['AP Path Loss: ', pathLossAP2client, 'path Loss Jammer to client: ', pathLossJammer2client, 'AP power1: ', APpower1])
    outputWriter.writerow(['AP power 6: ', APpower6, 'AP power 11: ', APpower11, 'RSSI error:', RSSIerror])
    outputWriter.writerow(['Test', 'APattenuation', 'jammerPowerLevel', 'RSSI', 'jammerRSSI', 'CtoI', 'pseudorange',  'UDP_PER', 'UDP_BW', 'TCP_BW'])
    
    print("Processing Baseline Test. Resetting the Jammer.")
    reset_jammerGenerator()
    print(" Manually Set AP wi-Fi channel = 11, MIMO = off, IEEE802.11bgn, 100% power.")
    print(" On the client start >> iperf3 -s . \n")
    print("Press 1 when  you have complied:")
    num = input() 
    for RSSI in [ -55, -65, -75, -85] :
        APattenuation = int(APpower11 - pathLossAP2client -RSSI)
        if (APattenuation < 0):    #lowest attenuation setting
            APattenuation = 0
        Vaunix_init(APattenuation) 
        print ("Working on the RSSI =  ", RSSI, "dBm loop ")
        pseudorange = float(10 ** ((-RSSI + APpower11 -32.4 - 20*np.log10(2500))/20))*1000
        pseudorangeR = round(pseudorange,2)
        tcpBW = get_iPerf_TCP()
		time.sleep(10)
        UDP_PER,udpBW = get_iPerf_UDP()
		time.sleep(10)
        outputWriter.writerow(['BaseLine', APattenuation ,'-', RSSI, 'OFF', '-',pseudorangeR, UDP_PER, udpBW, tcpBW])
        if ((UDP_PER == 100) and (tcpBW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
           
    wiFiChannel = 11
    print("Load LTE uplink profile in /var/user/Unicod/TDD_UL_40PCT on jammer Generator.")
    print("Press 1 when  you have complied:")
    num = input()
    print(num)
    for RSSI in [-55, -65, -75, -85] :
        APattenuation = int(APpower11 - pathLossAP2client -RSSI)
        if (APattenuation < 0):    #lowest attenuation setting
            APattenuation = 0
        Vaunix_init(APattenuation)
        print ("Working on the RSSI =  ", RSSI, "dBm loop ")
        for jammerPowerLevel in range (-25,11) :
           setJammerGenerator('2.490', str(jammerPowerLevel))
           jammerRSSI = float(jammerPowerLevel - pathLossJammer2client)
           CtoI = float(RSSI - jammerRSSI)
           pseudorange = float(10 ** ((-RSSI + APpower11 -32.4 - 20*np.log10(2500))/20))*1000
           pseudorangeR = round(pseudorange,2)
           if( jammerPowerLevel == -25):
               print (" Commencing 30 sec stabilization for LTE UL v. 11.")
               time.sleep(30)
           tcpBW = get_iPerf_TCP()
		   time.sleep(10)
           UDP_PER,udpBW = get_iPerf_UDP()
		   time.sleep(10)
           outputWriter.writerow(['LTE_UL v.11', APattenuation, jammerPowerLevel, RSSI, jammerRSSI, CtoI, pseudorangeR, UDP_PER, udpBW, tcpBW])
           if ((UDP_PER == 100) and (tcpBW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
               
    outputFile.close()
    
# main program starts here
main()
reset_jammerGenerator()

# attenuator testing.......
#print("Give the attenuation in dB")
#attenuation = int(input())  # set attenuator to input dB.
#print(attenuation)
#Vaunix_init(attenuation)
#num = input()
#print(" testing 60 sec TCP and 60 sec UDP")
#Vaunix_init(20)     # testing iPerf
#i = 10
#while(i>0):
#    get_iPerf_TCP()
#    get_iPerf_UDP()
#    i -= 1

print ("Done! We now return control of your instruments to you.")
