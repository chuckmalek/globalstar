
#
#  GlobalStar LTE and WiFi vs. WiFi automated interference testing Intelligence with *.csv output.
# by Charles J Malek, P.E. Property of Roberson & Associates. October 2017
# ver 1.0a on 26 October 2017. added iPerf exception handling and breaks.
# ver 1.0b 26 October. jammer range and RSSI missing terms.
# ver 1.0c 27 October adjusted AP attenuation list
# ver 1.0c 30 Oct 2017 stabilization waiting time for first lowest level jammer.
#
import sys,csv,datetime, string, os, telnetlib
from ctypes import * 
import scipy
import numpy as np
import subprocess as SP
import time

def get_iPerf_TCP():
    cmdTCP = ['iperf3.exe', '-c', '192.168.1.7', '-O 3', '-t 20']  # TCP/IP is the default
    try:
        output1=SP.check_output(cmdTCP)
    except FileNotFoundError as e:
        print('Error opening %s, must be reachable from path env. variable ' % cmd[0],e)
    except:
        ("iPerf TCP fails.\n")
        return(0)
    outputstrings=output1.decode('utf-8').split('\n')
#   for ss in outputstrings:
#      print(ss)
#    print (outputstrings[16],"\n\n")  
    BW = float(outputstrings[16].split()[6])
    print("BW = ", BW, " Mbps")
    return(BW)
 
def get_iPerf_UDP():
    cmdUDP = ['iperf3.exe', '-c', '192.168.1.7', '-u', '-b 24M'] # UDP at 24 Mbits/sec offered load
    try:
        output1=SP.check_output(cmdUDP)
    except FileNotFoundError as e:
        print('Error opening %s, must be reachable from path env. variable ' % cmd[0],e)
    except:
        print("iPerf for UDP returns 100% PER.\n")
        return(100)
    outputstrings=output1.decode('utf-8').split('\n')
#   for ss in outputstrings:
#     print(ss)
    udpPER = float(outputstrings[15].split()[11].replace("%)", "").replace("(", ""))
    print("udpPER =", udpPER, "%")
    return(udpPER)      

def setJammerGenerator(freq, power):
# sets power and freq on the SMBV-100A vector signal jammer generator
    host = "192.168.1.5"
    port = "5025"
    telnet = telnetlib.Telnet(host,port)
#    print(telnet)
    print ("configuring generator power to", power, "dBm and freq to",freq, "GHz \n")
    telnet.write(b"source1:pow" + b" " + power.encode('ascii') + b"\r\n")
    telnet.write( b"source1:freq" + b" " + freq.encode('ascii') + b"GHz" + b"\r\n")
    telnet.close()  
    
def reset_jammerGenerator():
    # resets the SMBV-100A vector signal jammer generator
    host = "192.168.1.5"
    port = "5025"
    telnet = telnetlib.Telnet(host,port)
    print(telnet)
    print ("Resetting the SMBV-100A")
    telnet.write(b"*RST\n")
    telnet.close()
    
def recallJammerGenerator(savedBuffer, pathname):
# recalls a saved pathname on the SMBV-100A vector signal jammer generator
    host = "192.168.1.5"
    port = "5025"
    telnet = telnetlib.Telnet(host,port)
    print(telnet)
    print ("recalling jammer Generator saved file: ", savedBuffer, " \n")
    telnet.write(b"MMEM:LOAD:STAT " + savedBuffer.encode('ascii') + b" " + pathname.encode('ascii') + b"\r\n")
    telnet.write( b"*RCL " + savedBuffer.encode('ascii')  + b"\r\n")
    telnet.close()  
    print("save file on jammer Generator recalled.")
 
def Vaunix_init(attenuation):
# The LDA-602 has a 60 dB range,uses 0.5 dB steps,has an approx 7.5 dB insertion Loss at 2.4 GHz.               
    vnx = cdll.VNX_atten64
    vnx.fnLDA_SetTestMode(0)        # turn off the DLL’s simulation test mode
    DeviceIDArray = c_int * 64      # the array just has to be big enough for the number of devices you expect – 20 is fine, 64 is probably more than you need
    Devices = DeviceIDArray()
    vnx.fnLDA_GetNumDevices()      # tell the DLL to go out and find the hardware, it returns the number of devices found
    # at this point you can use the handles stored in the array to get information about each connected device
    vnx.fnLDA_GetDevInfo(Devices)    # get a list of active device handles from the DLL
    vnx.fnLDA_GetSerialNumber(Devices[0])   # get the serial number of the first connected device
    # for each device you want to use, init the device, or devices. My example assumes two devices
    vnx.fnLDA_InitDevice(Devices[0])    #initialize attenuation
    vnx.fnLDA_SetAttenuation(Devices[0], attenuation * 4)  # the DLL uses an integer value with units of .5 db, so the value is multiplied by 4
    # the python ctypes library does the conversion from python data types to integers
    print ("configuring Vaunix attenuator to ", attenuation, "dB ")
    vnx.fnLDA_CloseDevice(Devices[0])
    
def main():
# Greetings. The testing intelligence asks for human measurement input.
    print("\n\n")
    print("We welcome you to the GlobalStar terrestrial Testing Intelligence! \n")
    print("Please give the model of Residential AP. \n\n")
    ResidentialAP = input()
    print("Specify the AP to client distance in meters:")
    d_APtoClient = float(input())
    print("Specify the jammer to client distance in meters:")
    d_jammerToClient = float(input())
    print("Specify the AP to client insertionLoss, path Loss, plus antenna Gains:")
    pathLossAP2client = float(input())
    print("Specify the jammer to client insertion Loss, path Loss, plus antenna Gains:")
    pathLossJammer2client = float(input())
    print("Specify AP channel 1 power output in dBm:")
    APpower1 = float(input())
    print("Specify AP channel 6 power output in dBm:")
    APpower6 = float(input())    
    print("Specify AP channel 11 power output in dBm:")
    APpower11 = float(input())
    
    print(sys.version) 
    dt= datetime.datetime.now()
    print(dt)
    
    outputFile = open('globalStarDay2.csv','w', newline='')
    outputWriter = csv.writer(outputFile)
    outputWriter.writerow(['GlobalStar', '2.4 GHz', 'Residential Test']) 
    outputWriter.writerow(['stardate = ', dt.year,dt.month,dt.day,dt.hour,dt.minute])
    outputWriter.writerow([" "])
    outputWriter.writerow(['Residential AP: ', ResidentialAP, 'distance AP to client: ', d_APtoClient,'distance jammer to client:', d_jammerToClient])
    outputWriter.writerow(['AP Path Loss: ', pathLossAP2client, 'path Loss Jammer to client: ', pathLossJammer2client, 'AP power1: ', APpower1])
    outputWriter.writerow(['AP power 6: ', APpower6, 'AP power 11: ', APpower11])
    outputWriter.writerow(['Test', 'APattenuation', 'jammerPowerLevel', 'RSSI', 'jammerRSSI', 'CtoI', 'pseudorange', 'UDP_jitter', 'UDP_PER','TCP_BW'])
    
    print("Processing Baseline Test. Resetting the Jammer.")
    reset_jammerGenerator()
    print(" Manually Set AP wi-Fi channel = 11, MIMO = off, IEEE802.11bg, 100% power.")
    print(" On the client start >> iperf3 -s . \n")
    print("Press 1 when  you have complied:")
    num = input() 
    for APattenuation in [ 30, 28, 26, 24, 22, 20, 18, 16, 14, 12, 10, 8, 6, 4, 2, 0] :
        Vaunix_init(APattenuation)
        RSSI = float(APpower11 - APattenuation - pathLossAP2client)
        pseudorange = float(10 ** ((-RSSI + APpower11 -32.4 - 20*np.log10(2500))/20))*1000
        pseudorangeR = round(pseudorange,2)
        BW = get_iPerf_TCP()
        UDP_PER = get_iPerf_UDP()
        outputWriter.writerow(['BaseLine', APattenuation ,'-', RSSI, 'OFF', '-',pseudorangeR, '-', UDP_PER, BW])
        if ((UDP_PER == 100) or (BW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
           
    wiFiChannel = 11
    recallJammerGenerator('1', '/var/user/globalstarWiFi')         # experimental file recall
    print("Load WiFi profile in /var/user/globalstarWiFi on jammer Generator.")
    print("Press 1 when  you have complied:")
    num = input()
    print(num)
    for APattenuation in [4, 10, 20, 30] :
        Vaunix_init(APattenuation)
        for jammerPowerLevel in range (-25,11) :
           setJammerGenerator('2.437', str(jammerPowerLevel))
           RSSI = float(APpower11  - APattenuation - pathLossAP2client)
           jammerRSSI = float(jammerPowerLevel - pathLossJammer2client)
           CtoI = float(RSSI - jammerRSSI)
           pseudorange = float(10 ** ((-RSSI + APpower11 -32.4 - 20*np.log10(2500))/20))*1000
           pseudorangeR = round(pseudorange,2)
           if( jammerPowerLevel == -25):
               print (" Commencing 30 sec stabilization for WiFi Chan 6 v. 11.")
               time.sleep(30)
           BW = get_iPerf_TCP()
           UDP_PER = get_iPerf_UDP()
           outputWriter.writerow(['wiFi Chan 6 v.11', APattenuation, jammerPowerLevel, RSSI, jammerRSSI, CtoI, pseudorangeR, '-', UDP_PER, BW])
           if ((UDP_PER == 100) or (BW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
               
    print("Load LTE uplink profile in /var/user/Unicod/TDD_UL_40PCT on jammer Generator:")
    print("Press 1 when  you have complied:")
    num = input()
    print(num)
    for APattenuation in [4, 10, 20, 30] :
        Vaunix_init(APattenuation)
        for jammerPowerLevel in range (-25,11) :
           setJammerGenerator('2.490', str(jammerPowerLevel))
           RSSI = float(APpower11 - APattenuation - pathLossAP2client)
           jammerRSSI = float(jammerPowerLevel - pathLossJammer2client)
           CtoI = float(RSSI - jammerRSSI)
           pseudorange = float(10 ** ((-RSSI + APpower11 -32.4 - 20*np.log10(2500))/20))*1000
           pseudorangeR = round(pseudorange,2)
           if( jammerPowerLevel == -25):
               print (" Commencing 30 sec stabilization for LTE uplink v.11.")
               time.sleep(30)
           BW = get_iPerf_TCP()
           UDP_PER = get_iPerf_UDP()
           outputWriter.writerow(['LTE uplink v.11', APattenuation, jammerPowerLevel, RSSI, jammerRSSI, CtoI, pseudorangeR, '-', UDP_PER, BW])          
           if ((UDP_PER == 100) or (BW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
           
    print("Load LTE downlink profile in /var/user/Unicod/TDD_DL_80PCT on jammer Generator:")
    print("Press 1 when  you have complied:")
    num = input()
    print(num)
    for APattenuation in [4, 10, 20, 30] :
        Vaunix_init(APattenuation)
        for jammerPowerLevel in range (-25,11) :
           setJammerGenerator('2.490', str(jammerPowerLevel))
           RSSI = float(APpower11 - APattenuation - pathLossAP2client)
           jammerRSSI = float(jammerPowerLevel - pathLossJammer2client)
           CtoI = float(RSSI - jammerRSSI)
           pseudorange = float(10 ** ((-RSSI + APpower11 -32.4 - 20*np.log10(2500))/20))*1000
           pseudorangeR = round(pseudorange,2)
           if(jammerPowerLevel == -25):
               print (" Commencing 30 sec stabilization for LTE downlink v.11.")
               time.sleep(30)
           BW = get_iPerf_TCP()
           UDP_PER = get_iPerf_UDP()
           outputWriter.writerow(['LTE downlink v.11', APattenuation, jammerPowerLevel,RSSI, jammerRSSI, CtoI, pseudorangeR, '-', UDP_PER, BW])  
           if ((UDP_PER == 100) or (BW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
           
    wiFiChannel = 6
    print(" Manually Set AP wi-Fi channel = 6, MIMO = off, IEEE802.11bg, 100% power.")
    print("Press 1 when  you have complied:")
    num = input()
    print(num)
    for APattenuation in [4, 10, 20, 30] :
        Vaunix_init(APattenuation)
        for jammerPowerLevel in range (-25,11) :
           setJammerGenerator('2.490', str(jammerPowerLevel))
           RSSI = float(APpower6 -  APattenuation - pathLossAP2client)
           jammerRSSI = float(jammerPowerLevel - pathLossJammer2client)
           CtoI = float(RSSI - jammerRSSI)
           pseudorange = float(10 ** ((-RSSI + APpower6 -32.4 - 20*np.log10(2500))/20))*1000
           pseudorangeR = round(pseudorange,2)
           if( jammerPowerLevel == -25):
               print (" Commencing 30 sec stabilization for LTE downlink v.6.")
               time.sleep(30)
           BW = get_iPerf_TCP()
           UDP_PER = get_iPerf_UDP()
           outputWriter.writerow(['LTE downlink v.6', APattenuation, jammerPowerLevel,RSSI, jammerRSSI, CtoI, pseudorangeR, '-', UDP_PER, BW])          
           if ((UDP_PER == 100) or (BW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
           
    wiFiChannel = 1
    print(" Manually Set AP wi-Fi channel = 1, MIMO = off, IEEE802.11bg, 100% power.")
    print("Press 1 when  you have complied:")
    num = input()
    print(num)
    for APattenuation in [4, 10, 20, 30] :
        Vaunix_init(APattenuation)
        for jammerPowerLevel in range (-25,11) :
           setJammerGenerator('2.490', str(jammerPowerLevel))
           RSSI = float(APpower1 -  APattenuation - pathLossAP2client)
           jammerRSSI = float(jammerPowerLevel - pathLossJammer2client)
           CtoI = float(RSSI - jammerRSSI)
           pseudorange = float(10 ** ((-RSSI + APpower1 -32.4 - 20*np.log10(2500))/20))*1000
           pseudorangeR = round(pseudorange,2)
           if( jammerPowerLevel == -25):
               print (" Commencing 30 sec stabilization for LTE downlink v.1.")
               time.sleep(30)
           BW = get_iPerf_TCP()
           UDP_PER = get_iPerf_UDP()
           outputWriter.writerow(['LTE downlink v.1', APattenuation, jammerPowerLevel,RSSI, jammerRSSI, CtoI, pseudorangeR, '-', UDP_PER, BW])
           if ((UDP_PER == 100) or (BW == 0)):
               print(" iPerf3 exception! On the client  RESTART >> iperf3 -s\n")
               print("Press 1 when  you have complied:")
               num = input()
               break
    outputFile.close()
    
# main program starts here
main()
# attenuator testing.......
#print("Give the atenuation in dB")
#attenuation = int(input())  # set attenuator to input dB.
#print(attenuation)
#Vaunix_init(attenuation)
reset_jammerGenerator()
#Vaunix_init(10)     # testing iPerf
#get_iPerf_UDP()
print ("Done! We now return control of your instruments to you.")
